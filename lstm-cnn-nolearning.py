# univariate bidirectional lstm example
from numpy import array, append
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import TimeDistributed
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D

import arg_check
from inspect import currentframe, getframeinfo
import errno
from tools import stdo, time_log, Time_Flag, time_history
import pandas as pd

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


def get_dataset(path):
    """
        This function returns dataset with given path
        Parameters;
            path for path of dataset
        returns pandas.core.frame.DataFrame type variable
    """
    try:
        return pd.read_csv(path)
    except OSError as oserror:
        if errno.ENOENT == oserror.errno:
            stdo(3, "File not found. " + oserror.__str__(),  getframeinfo(currentframe() ) )
        else:
            stdo(3, "Error Occured while opening '{}' dataset file: {}".format(path, oserror.__str__() ),  getframeinfo( currentframe() ))
        stdo(1, "--- --- ---")
        exit(-1)
    except UnicodeDecodeError as error:
        stdo(3, "A Unicode Decode Error is typically caused by not specifying the encoding of the file, and happens when you have a file with non-standard characters. For a quick fix, try opening the file in Sublime Text, and re-saving with encoding ‘UTF-8’.: {}".format( error.__str__() ),  getframeinfo( currentframe() ))
        exit(-1)
    except pd.parser.CParserError:
        try:
            return pd.read_csv(path, engine="python")
        except Exception as error:
            stdo(3, "An error occured while opening dataset file and can't recovered: {}".format( error.__str__() ),  getframeinfo( currentframe() ))
            exit(-1)
    except Exception as error:
        stdo(3, "An error occured while opening '{}' dataset file: {}".format(path, error.__str__() ),  getframeinfo( currentframe() ))
        exit(-1)


# split a univariate sequence
def split_sequence(sequence, n_steps):
    X, y = list(), list()
    for i in range(len(sequence)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the sequence
        if end_ix > len(sequence) - 1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = sequence[i:end_ix], sequence[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return array(X), array(y)


def print_data(premessage = "", list_of_data = None, split = 4):
    if list_of_data is not None:
        str_list_of_data = "\t\t|"
        n = 0
        for data in list_of_data:
            if n == split:
                str_list_of_data += "\n\t\t|"
                n = 0
            n += 1
            str_list_of_data += "   ->{:7}".format(data)
    message = premessage + str_list_of_data
    stdo(1, message )


def main():
    time_log("LSTM-CNN")
    arg_check.control()
    arg_check.arg_info()

    # Read data from file 'filename.csv'
    # (in the same directory that your python process is based)
    # Control delimiters, rows, column names with read_csv (see later)
    dataset = get_dataset(arg_check.arguments["dataset_path"])

    # Preview the first 5 lines of the loaded data
    # print(dataset.head())

    for data in dataset.values:
        if data[1] == "Turkey":
            turkey_data = data

    # define input sequence
    temp_raw_seq = turkey_data[4:].tolist()

    raw_seq = list()
    is_firsts_cleaned = False
    # Zero Elimination
    for data in temp_raw_seq:
        if data != 0 or not is_firsts_cleaned:
            raw_seq.append(data)
            is_firsts_cleaned = True
    stdo(1, "First Zero's eliminated from raw data sequence.")

    print_data("Processed Raw Sequence Data ({}):\n".format(len(raw_seq)), raw_seq, split = 4 )

    train_data = raw_seq.copy()

    # univariate cnn lstm example
    # choose a number of time steps
    n_steps = 4

    # split into samples
    X, y = split_sequence(train_data, n_steps)

    # reshape from [samples, timesteps] into [samples, subsequences, timesteps, features]
    samples = X.shape[0]
    n_seq = 2
    n_steps = 2
    n_features = 1
    # n_steps = X.shape[1]
    X = X.reshape((samples, n_seq, n_steps, n_features))

    # define model
    model = Sequential()
    model.add(TimeDistributed(
                Conv1D(filters = 64, kernel_size = 1, activation='relu'),
                    input_shape = (None, n_steps, n_features) ))
    model.add(TimeDistributed(MaxPooling1D(pool_size = 2)))
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(50, activation = 'relu'))
    model.add(Dense(1))
    model.compile(optimizer = 'adam', loss = 'mse')

    # fit model
    if arg_check.arguments["epochs"] == -1:
        epochs = int(X.size * y.size / 2)
    else:
        epochs = arg_check.arguments["epochs"]

    model.fit(X, y, epochs = epochs, verbose = arg_check.arguments["verbose"])

    # demonstrate prediction
    number_of_data = (-n_steps * n_seq)
    original_test_input = array(train_data[number_of_data - 1: -1])
    raw_test_input = original_test_input.copy()

    predictions = array(list()).astype(int)
    for predict_time in range(arg_check.arguments["prediction"]):
        samples = 1
        test_input = raw_test_input.reshape((samples, n_seq, n_steps, n_features))

        current_predictions = model.predict(test_input, verbose = arg_check.arguments["verbose"], steps = n_steps)
        current_predictions = current_predictions[0].astype(int)
        # print("\n\t{}\n".format(current_predictions))

        # For forward predictions - more than 1 time step prediction
        # print("\n\t{}\n".format(raw_test_input))
        if predict_time != 1:
            raw_test_input = append(raw_test_input[number_of_data + 1:], current_predictions)
        else:
            raw_test_input = append(raw_test_input[number_of_data + 1:], train_data[-1])

        predictions = append(predictions, current_predictions)
        # print("\n\t{}\n".format(raw_test_input))

    time_log("LSTM-CNN", Time_Flag.End)
    stdo(1, "{:.3f} time passed".format( time_history["LSTM-CNN"][Time_Flag.Passed]) )
    print_data("Input Data (Last {} Day(s)):\n".format(n_steps * n_seq), raw_seq[number_of_data - 1:-1], split = 4 )
    print_data("Prediction Result(s)({}):\n".format(len(predictions)), predictions, split = 4 )


main()
