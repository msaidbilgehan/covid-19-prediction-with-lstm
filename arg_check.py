# IMPORTS
from tools import stdo    # For properly print out
import argparse


# GLOBALS
arguments_output = None
arguments = None


# FUNCTIONS
# https://stackoverflow.com/questions/15753701/how-can-i-pass-a-list-as-a-command-line-argument-with-argparse
def control():
    global arguments

    aP = argparse.ArgumentParser()  # Parse arguments

    aP.add_argument("-d", "--dataset-path", required = True,
                    help = "Dataset CSV file path.")

    aP.add_argument("-p", "--prediction", default = 1, type = int,
                    help = "Prediction time in forward sequence. (Default: 1)")

    aP.add_argument("-e", "--epochs", default = -1, type = int,
                    help = "Prediction epochs for model fitting, recommended -1 which is calculated automatically. Should be at least 1500. (Default: -1)")

    aP.add_argument("-v", "--verbose", default = 0, type = int,
                    help = "Output level such as 0, 1, 2 for no output, info and warning. (Default: 0)")

    arguments = vars(aP.parse_args())
    return 0


def output_args(only_cache = False):
    # https://docs.python.org/3/tutorial/datastructures.html
    # https://stackoverflow.com/questions/9453820/alternative-to-python-string-item-assignment

    global arguments, arguments_output

    arguments_output = "Given Arguments:\n"
    for arg in arguments:
        arguments_output += "\t\t|- {} : {}\n".format(arg, arguments[arg])

    if not only_cache:
        stdo(1, arguments_output)

    return 0


def arg_info():
    output_args()
    return 0
