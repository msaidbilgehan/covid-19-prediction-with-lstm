# univariate bidirectional lstm example
import numpy
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Bidirectional

import arg_check
from inspect import currentframe, getframeinfo
import errno
from tools import stdo, time_log, Time_Flag, time_history
import pandas as pd


import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


# split a univariate sequence
def split_sequence(sequence, n_steps):
    X, y = list(), list()
    for i in range(len(sequence)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the sequence
        if end_ix > len(sequence) - 1:
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = sequence[i:end_ix], sequence[end_ix]
        X.append(seq_x)
        y.append(seq_y)
    return numpy.array(X), numpy.array(y)


def print_data(premessage = "", list_of_data = None, split = 4):
    if list_of_data is not None:
        str_list_of_data = "\t\t|"
        n = 0
        for data in list_of_data:
            if n == split:
                str_list_of_data += "\n\t\t|"
                n = 0
            n += 1
            str_list_of_data += "   ->{:7}".format(data)
    message = premessage + str_list_of_data
    stdo(1, message )


def main():
    time_log("LSTM-Vanilla")
    arg_check.control()
    arg_check.arg_info()

    # Read data from file 'filename.csv'
    # (in the same directory that your python process is based)
    # Control delimiters, rows, column names with read_csv (see later)
    try:
        dataset = pd.read_csv(arg_check.arguments["dataset_path"])
    except OSError as oserror:
        if errno.ENOENT == oserror.errno:
            stdo(3, "File not found. " + oserror.__str__(),  getframeinfo(currentframe() ) )
        else:
            stdo(3, "Error Occured while opening dataset file: {}".format( oserror.__str__() ),  getframeinfo( currentframe() ))
        stdo(1, "--- --- ---")
        exit(-1)
    except UnicodeDecodeError as error:
        stdo(3, "A Unicode Decode Error is typically caused by not specifying the encoding of the file, and happens when you have a file with non-standard characters. For a quick fix, try opening the file in Sublime Text, and re-saving with encoding ‘UTF-8’.: {}".format( error.__str__() ),  getframeinfo( currentframe() ))
        exit(-1)
    except pd.parser.CParserError:
        try:
            dataset = pd.read_csv(arg_check.arguments["dataset_path"], engine="python")
        except Exception as error:
            stdo(3, "An error occured while opening dataset file and can't recovered: {}".format( error.__str__() ),  getframeinfo( currentframe() ))
            exit(-1)
    except Exception as error:
        stdo(3, "An error occured while opening dataset file: {}".format( error.__str__() ),  getframeinfo( currentframe() ))
        exit(-1)

    # Preview the first 5 lines of the loaded data
    dataset.head()

    for data in dataset.values:
        if data[1] == "Turkey":
            turkey_data = data

    # define input sequence
    temp_raw_seq = turkey_data[4:].tolist()

    raw_seq = list()
    # Zero Elimination
    for data in temp_raw_seq:
        if data != 0:
            raw_seq.append(data)

    train_size = len(raw_seq) - arg_check.arguments["test_size"]
    test_size = arg_check.arguments["test_size"]

    raw_seq = numpy.array(raw_seq)
    train_raw_seq = raw_seq[:train_size].copy()

    stdo(1, "Zero's eliminated from raw data sequence.")

    for predict in range(arg_check.arguments["prediction"]):
        # choose a number of time steps
        # n_steps = arg_check.arguments["steps"]
        n_steps = test_size

        # split into samples
        X, y = split_sequence(train_raw_seq, n_steps)

        # reshape from [samples, timesteps] into [samples, timesteps, features]
        n_features = 1
        X = X.reshape( (X.shape[0], X.shape[1], n_features) )

        # define model
        model = Sequential()
        model.add( Bidirectional( LSTM(50, activation = 'relu'), input_shape = (n_steps, n_features) ) )
        model.add( Dense(1) )
        model.compile(optimizer='adam', loss='mse')

        # fit model
        model.fit(X, y, epochs = 200, verbose = arg_check.arguments["verbose"])

        # demonstrate prediction
        x_input = numpy.array(train_raw_seq[-test_size:])
        x_input = x_input.reshape((1, n_steps, n_features))

        predictions = model.predict(x_input, verbose = arg_check.arguments["verbose"], steps = n_steps)

        train_raw_seq = numpy.append( train_raw_seq, int(predictions[0][0]) )

    ground_truth = raw_seq[-arg_check.arguments["test_size"]:]
    predicted_data = train_raw_seq[-arg_check.arguments["prediction"]:]

    time_log("LSTM-Vanilla", Time_Flag.End)
    stdo(1, "{:.3f} time passed".format( time_history["LSTM-Vanilla"][Time_Flag.Passed]) )
    print_data("Processed Raw Sequence Data ({}):\n".format(len(raw_seq[:train_size])), raw_seq[:train_size], split = 4 )
    print_data("Ground Thruth Data:\n", ground_truth, split = 4 )
    print_data("Prediction Result(s)({}):\n".format(len(predicted_data)), predicted_data, split = 4 )


main()
