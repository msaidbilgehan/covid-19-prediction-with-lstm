import time
import os
from glob import glob
from inspect import currentframe, getframeinfo
from enum import Enum


class Time_Flag(Enum):
    Start = 0
    End = 1
    Passed = -1


time_struct = {
    "start": 0,
    "end": 0,
    "passed": 0
}

time_history = {}


def stdo(flag, string = "No Info Found", frameinfo = "No Info Found"):  # arg1 info warn ve error dan hangisi olduğunu belirliyor arg2 ise yazılacak output

    # Proper Time Parsing
    currentTime = ""
    for part in get_time().split("_"):
        if len(part) != 2:
            currentTime = currentTime + "0" + part[0] + ":"
        else:
            currentTime = currentTime + part + ":"
    currentTime = currentTime[:-1]

    if string == "No Info Found":
        print("""\n[{0}] - ERR: Invalid arguments!
                       |- FLAG: {1}
                       |- FRAMEINFO: {2}
                       '- STRING: {3}\n""".format( currentTime, flag, frameinfo, str(string) ) )

    elif flag == 1:
        print("[{0}] - INF: {1}".format( currentTime, str(string) ) )

    elif flag == 2:
        print("[{0}] - WRN: {1}".format( currentTime, str(string) ) )

    elif flag == 3:
        if frameinfo != "No Info Found":
            print("""\n[{0}] - ERR:   Invalid arguments!
                    |- PATH: {1} ({2})
                    '- MESSAGE: {3}\n""".format( currentTime, str(frameinfo.filename), str(frameinfo.lineno), str(string) ) )
        else:
            print("""\n[{0}] - ERR: Invalid arguments!
                           |- PATH: {1}
                           '- MESSAGE: {2}\n""".format( currentTime, frameinfo, str(string) ) )

    return 0


def get_time(level=0):
    if level == 0 or level == 1:
        if level == 0:  # To get only clock
            cTime = time.localtime( time.time() )[3:6]

        elif level == 1:  # To get only date
            cTime = time.localtime( time.time() )[:3]

        sCTime = ""
        for part in cTime:
            sCTime += str( part ) + "_"
        return sCTime[:-1]

    elif level == 2:  # To get date-clock (For file names)
        sCTime = "{0}-{1}".format( get_time(level=1), get_time(level=0) )

    elif level == 3:  # To get date | clock (For output logs)
        sCTime = "{0} | {1}".format( get_time(level=1), get_time(level=0) )

    else:
        return ""
    return sCTime


def get_OS():
    OS = ""

    if(os.name == "nt"):
        OS = "W"    # Windows
    else:
        OS = "P"    # Posix (Linux etc...)

    return OS


def time_log(id, option = Time_Flag.Start):
    global time_history, time_struct

    if len(time_history) == 0:
        time_history[id] = time_struct.copy()
    elif id not in time_history:
        time_history[id] = time_struct.copy()

    if option == Time_Flag.End and time_history[id][Time_Flag.Start] == 0:
        stdo(2, "Start time is not initilized. Taken time will be saved as start time.")
        option = Time_Flag.Start

    time_history[id][option] = time.time()  # To count program start time

    if option == Time_Flag.End:
        time_history[id][Time_Flag.Passed] = ( time_history[id][Time_Flag.End] - time_history[id][Time_Flag.Start] )


def list_files(path = "", name = "*", extensions = ["png"], recursive = False, verbose = True):
    # https://mkyong.com/python/python-how-to-list-all-files-in-a-directory/
    try:
        files = list()
        if recursive:
            if path[-1] != "/":
                path = path + "/"
        else:
            if path[-1] == "/":
                path = path[:-1]

        for extension in extensions:
            files.extend([f for f in glob(path + "**/{}.{}".format(name, extension), recursive = recursive)])

        """ RECURSIVE
        for path in files:
            if path.split("/")[-2] != ""
        """
        if verbose:
            output = "- {}".format(path)
            for subPath in files:
                subPath = subPath.replace(path, "")
                output += "\n"
                for i in range(len(path.split("/"))):
                    output += "\t"
                output += "\t|- {}".format(subPath)
            stdo(1, output)
            stdo(1, "{} files found".format( len(files) ))

        return files

    except Exception as error:
        stdo(3, "An error occured while working in list_files function -> " + error.__str__(),  getframeinfo(currentframe() ) )
        stdo(1, "--- --- ---")
    return None


def name_parsing(filePath, separate = False, separator = ".", maxSplit = -1):    # parsing name from file path
    file = filePath.split("/")[-1]
    extension = file.split(".")[-1]
    name = file.strip( "." + extension)
    if separate:
        # https://www.programiz.com/python-programming/methods/string/strip
        name = name.split(separator, maxSplit)
    return name, extension
