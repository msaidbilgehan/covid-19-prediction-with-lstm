# COVID 19 Prediction with LSTM

Written with Python

Tested on Python 3.5.3 and DeepinOS 15.11 Stable

Necessary dependencies will be stored in "requirements.txt" file. Can be
installed with using;

    pip install -r requirements.txt

Virtual environment is recommended.

Currently, works with getting Turkey data which is monthly confirmed cases in [time_series_covid19_confirmed_global](https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases) and gives it as raw sequence.

Basically runs with given command below;

    python3 lstm-cnn-learning.py -d Datasets/time_series_covid19_confirmed_global.csv

## Snippets

### 3 Days Prediction

    python3 lstm-cnn-learning.py -d Datasets/time_series_covid19_confirmed_global.csv -p 3

### 3 Days Prediction with Verbose Mode Activated

    python3 lstm-cnn-learning.py -d Datasets/time_series_covid19_confirmed_global.csv -p 3 -v 1

### 3 Days Prediction with Custom Epochs
*(To get best value, Do not use Epochs option)*

    python3 lstm-cnn-learning.py -d Datasets/time_series_covid19_confirmed_global.csv -p 3 -e 1500

# Usage

    lstm-cnn-learning.py [-h] -d DATASET_PATH [-t TEST_SIZE] [-rt RENDER_TIME]
                          [-p PREDICTION] [-v VERBOSE]

# Optional Arguments

    -h, --help            show this help message and exit

    -d DATASET_PATH, --dataset-path DATASET_PATH
                        Dataset CSV file path.

    -p PREDICTION, --prediction PREDICTION
                        Prediction time in forward sequence. 1 for 1 day, 10 for 10 days (Default: 1)

    -e EPOCHS, --epochs EPOCHS
                          Prediction epochs for model fitting, recommended -1
                          which is calculated automatically. Should be at least
                          1500. (Default: -1)

    -v VERBOSE, --verbose VERBOSE
                        Output level such as 0, 1, 2 for no output(default), info and
                        warning. (Default: 0)
